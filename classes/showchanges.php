<?php
include("bootstrapfunc.php");
include("dbtool.php");
include("showchangesfunc.php");
include("../config.php");
bootstraphead();
bootstrapbegin("Änderungen");
$menu=$_GET['menu'];
$showtyp=$_GET['showtyp'];
echo "<a href='showtab.php?menu=".$menu."'  class='btn btn-primary btn-sm active' role='button'>Zurück</a> ";
if ($showtyp=="NOSYNC") {
  update_nosync($database,$debug);
} else {
  echo "<a href='showchanges.php?menu=".$menu."&showtyp=NOSYNC'  class='btn btn-primary btn-sm active' role='button'>nosync</a> ";
  zeige_aenderungen_lokal($database,$debug);
}  
bootstrapend();
?>